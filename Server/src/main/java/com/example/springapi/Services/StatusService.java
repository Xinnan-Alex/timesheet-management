package com.example.springapi.Services;

import com.example.springapi.Models.Status;
import com.example.springapi.Models.User;
import com.example.springapi.Repo.StatusRepo;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class StatusService {

    private final StatusRepo statusRepo;

    @Autowired
    public StatusService(StatusRepo statusRepo) {
        this.statusRepo = statusRepo;
    }

    public List<Status> getStatus() {
        return statusRepo.findAll();
    }

    public Optional<Status> getStatusByName(String name){
        return statusRepo.findById(name);
    }

    public Status createStatus(@RequestBody Status status){
        return statusRepo.save(status);
    }

    @Transactional
    public Status updateStatus(String name,@RequestBody Status status){
        Status statusUpdated = statusRepo.findById(name).orElseThrow(()-> new IllegalStateException("Status with the name: " + name + " does not exists"));

        if (status.getName() != null && !status.getName().isEmpty() && !Objects.equals(statusUpdated.getName(),status.getName())){
            statusUpdated.setName(status.getName());
        }

        return statusRepo.save(statusUpdated);
    }

    public void deleteStatus(String name) {
        boolean exists = statusRepo.existsById(name);
        if (!exists){
            throw new IllegalStateException("Status with the name: " + name + " does not exists");
        }

        statusRepo.deleteById(name);
    }
}
