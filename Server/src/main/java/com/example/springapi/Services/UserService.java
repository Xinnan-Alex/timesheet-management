package com.example.springapi.Services;

import com.example.springapi.Models.User;
import com.example.springapi.Repo.UserRepo;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Objects;

@Service
public class UserService {

    private final UserRepo userRepo;

    @Autowired
    public UserService(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    public List<User> getUsers(){
        return userRepo.findAll();
    }

    public User getById(long id) {
        return userRepo.findById(id).orElseThrow(()-> new IllegalStateException("student with id " + id + " does not exists"));
    }

    public User createUser(@RequestBody User user){
        return userRepo.save(user);
    }

    public void deleteUser(long id){
        boolean exists = userRepo.existsById(id);
        if (!exists){
            throw new IllegalStateException("student with id " + id + " does not exists");
        }

        userRepo.deleteById(id);
    }

    @Transactional
    public User updateUser(Long id,@RequestBody User user){
        User updatedUser = userRepo.findById(id).orElseThrow(()-> new IllegalStateException("student with id " + id + " does not exists"));

        if (user.getFullName() != null && !user.getFullName().isEmpty() && !Objects.equals(updatedUser.getFullName(),user.getFullName())){
            updatedUser.setFullName(user.getFullName());
        }

        return userRepo.save(updatedUser);
    }
}
