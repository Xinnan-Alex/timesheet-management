package com.example.springapi.Services;

import com.example.springapi.Models.Status;
import com.example.springapi.Models.TimeSheet;
import com.example.springapi.Models.User;
import com.example.springapi.Repo.StatusRepo;
import com.example.springapi.Repo.TimeSheetRepo;
import com.example.springapi.Repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service
public class TimeSheetService {
    private final TimeSheetRepo timesheetRepo;
    private final UserRepo userRepo;
    private final StatusRepo statusRepo;

    @Autowired
    public TimeSheetService(TimeSheetRepo timesheetRepo, UserRepo userRepo, StatusRepo statusRepo) {
        this.timesheetRepo = timesheetRepo;
        this.userRepo = userRepo;
        this.statusRepo = statusRepo;
    }

    public List<TimeSheet> getTimeSheets(){
        return timesheetRepo.findAll();
    }

    public List<TimeSheet> getTimeSheets(String sortType, String sortBy){
        if (sortBy!=null && sortType!=null ) {
            if (sortBy.equalsIgnoreCase("fromtime")) {
                return switch (sortType.toLowerCase()) {
                    case "desc" -> timesheetRepo.findAllByOrderByFromTimeDesc();
                    case "asc" -> timesheetRepo.findAllByOrderByFromTimeAsc();
                    default -> timesheetRepo.findAll();
                };
            } else if (sortBy.equalsIgnoreCase("totime")) {
                return switch (sortType.toLowerCase()) {
                    case "desc" -> timesheetRepo.findAllByOrderByToTimeDesc();
                    case "asc" -> timesheetRepo.findAllByOrderByToTimeAsc();
                    default -> timesheetRepo.findAll();
                };
            }
        }

        return timesheetRepo.findAll();
    }

    public TimeSheet createTimeSheet(@RequestBody TimeSheet timeSheet) {
        //assuming SectorRepository available to this function
        User user =
                userRepo.findById(timeSheet.getUser().getId())
                        .orElseThrow(IllegalArgumentException::new);

        Status status =
                statusRepo.findById(timeSheet.getStatus().getName())
                        .orElseThrow(IllegalArgumentException::new);

        timeSheet.setUser(user); //Important! will save foreign key to table
        timeSheet.setStatus(status);
        return timesheetRepo
                    .save(new TimeSheet(
                            timeSheet.getProjectName(),
                            timeSheet.getTaskName(),
                            timeSheet.getFromTime(),
                            timeSheet.getToTime(),
                            timeSheet.getUser(),
                            timeSheet.getStatus())
                    );
    }

    public TimeSheet updateTimeSheet(long id,@RequestBody TimeSheet timeSheet) {
        TimeSheet updatedTimeSheet = timesheetRepo.findById(id).orElseThrow(()-> new IllegalStateException("timesheet with id " + id + " does not exists"));

        User user =
                userRepo.findById(timeSheet.getUser().getId())
                        .orElseThrow(IllegalArgumentException::new);

        Status status =
                statusRepo.findById(timeSheet.getStatus().getName())
                        .orElseThrow(IllegalArgumentException::new);

        timeSheet.setUser(user); //Important! will save foriegn key to table
        timeSheet.setStatus(status);

        //Update
        updatedTimeSheet.setProjectName(timeSheet.getProjectName());
        updatedTimeSheet.setTaskName(timeSheet.getTaskName());
        updatedTimeSheet.setFromTime(timeSheet.getFromTime());
        updatedTimeSheet.setToTime(timeSheet.getToTime());
        updatedTimeSheet.setStatus(timeSheet.getStatus());
        updatedTimeSheet.setUser(timeSheet.getUser());

        return timesheetRepo.save(updatedTimeSheet);
    }

    public void deleteTimeSheet(long id) {
        boolean exists = timesheetRepo.existsById(id);
        if (!exists){
            throw new IllegalStateException("timesheet with id " + id + " does not exists");
        }

        timesheetRepo.deleteById(id);
    }
}
