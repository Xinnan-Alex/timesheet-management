package com.example.springapi.Models;

import jakarta.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "timesheet")
public class TimeSheet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "timesheet_id")
    private long id;

    @Column(nullable = false)
    private String projectName;

    @Column(nullable = false)
    private String taskName;

    @Column(nullable = false)
    private LocalDateTime fromTime;

    @Column(nullable = false)
    private LocalDateTime toTime;

    @ManyToOne
    @JoinColumn(name = "assinged_to",nullable = false)
    private User user;

    @ManyToOne
    @JoinColumn(name = "status",nullable = false)
    private Status status;

    public TimeSheet(String projectName, String taskName, LocalDateTime fromTime, LocalDateTime toTime, User user, Status status) {
        this.projectName = projectName;
        this.taskName = taskName;
        this.fromTime = fromTime;
        this.toTime = toTime;
        this.user = user;
        this.status = status;
    }

    public TimeSheet() {

    }

    public long getId() {
        return id;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public LocalDateTime getFromTime() {
        return fromTime;
    }

    public void setFromTime(LocalDateTime fromTime) {
        this.fromTime = fromTime;
    }

    public LocalDateTime getToTime() {
        return toTime;
    }

    public void setToTime(LocalDateTime toTime) {
        this.toTime = toTime;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
