package com.example.springapi.Models;

import jakarta.persistence.*;

import java.util.List;

@Entity
@Table
public class Status {
    @Id
    @Column(name = "status_name")
    private String name;

    public Status(String name) {
        this.name = name;
    }

    public Status() {

    }

    @OneToMany(mappedBy = "status")
    private List<TimeSheet> timeSheet;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
