package com.example.springapi.Models;

import jakarta.persistence.*;
import org.hibernate.annotations.CreationTimestamp;

import java.sql.Time;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Entity
@Table
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private long id;

    @Column(nullable = false)
    private String fullName;

    @CreationTimestamp
    @Column(nullable = false)
    private LocalDateTime created_at;

    public User(String fullName) {
        this.fullName = fullName;
    }

    public User() {

    }

    public long getId() {
        return id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public LocalDateTime getCreated_at() {
        return created_at;
    }

    @OneToMany(mappedBy = "user")
    private List<TimeSheet> timeSheet;

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", fullName='" + fullName + '\'' +
                ", created_at=" + created_at +
                '}';
    }
}
