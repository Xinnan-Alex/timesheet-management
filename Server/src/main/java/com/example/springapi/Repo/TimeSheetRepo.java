package com.example.springapi.Repo;

import com.example.springapi.Models.TimeSheet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TimeSheetRepo extends JpaRepository<TimeSheet, Long> {
    List<TimeSheet> findAllByOrderByFromTimeAsc();

    List<TimeSheet> findAllByOrderByFromTimeDesc();

    List<TimeSheet> findAllByOrderByToTimeAsc();

    List<TimeSheet> findAllByOrderByToTimeDesc();


//    @Query("select t from time_sheet t order by t.from_time ASC")
//    List<TimeSheet> findAllTimeSheetsSortedByFromTimeASC();
//
//    @Query("select t from time_sheet t order by t.from_time DESC")
//    List<TimeSheet> findAllTimeSheetsSortedByFromTimeDESC();
//
//    @Query("select t from time_sheet t order by t.to_time ASC")
//    List<TimeSheet> findAllTimeSheetsSortedByToTimeASC();
//
//    @Query("select t from time_sheet t order by t.to_time DESC")
//    List<TimeSheet> findAllTimeSheetsSortedByToTimeDESC();
}
