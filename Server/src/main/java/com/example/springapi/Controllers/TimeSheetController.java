package com.example.springapi.Controllers;

import com.example.springapi.Models.TimeSheet;
import com.example.springapi.Services.TimeSheetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@RestController
@RequestMapping("api/v1/timesheets")
@CrossOrigin(origins = "http://localhost:4200")
public class TimeSheetController {

    private final TimeSheetService timeSheetService;

    @Autowired
    public TimeSheetController(TimeSheetService timeSheetService) {
        this.timeSheetService = timeSheetService;
    }

    @GetMapping
    public ResponseEntity<List<TimeSheet>> getTimeSheets(@RequestParam(name = "sortType",required = false) String sortType,@RequestParam(name = "sortBy",required = false) String sortBy){
        return new ResponseEntity<>(timeSheetService.getTimeSheets(sortType,sortBy), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<TimeSheet> createTimeSheets(@RequestBody TimeSheet timeSheet){
        System.out.println(timeSheet.getUser());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
        timeSheet.setFromTime(LocalDateTime.parse(timeSheet.getFromTime().toString(), formatter));
        timeSheet.setToTime(LocalDateTime.parse(timeSheet.getToTime().toString(), formatter));
        return new ResponseEntity<>(timeSheetService.createTimeSheet(timeSheet), HttpStatus.CREATED);
    }

    @PutMapping("{id}")
    public ResponseEntity<TimeSheet> updateTimeSheet(@PathVariable("id") long id, @RequestBody TimeSheet timeSheet){
        return new ResponseEntity<>(timeSheetService.updateTimeSheet(id,timeSheet),HttpStatus.OK);
    }
    //
    @DeleteMapping("{id}")
    public ResponseEntity<TimeSheet> deleteTimeSheet(@PathVariable("id") long id){
        timeSheetService.deleteTimeSheet(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
