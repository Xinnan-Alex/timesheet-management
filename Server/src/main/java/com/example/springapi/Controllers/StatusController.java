package com.example.springapi.Controllers;

import com.example.springapi.Models.Status;
import com.example.springapi.Models.TimeSheet;
import com.example.springapi.Services.StatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/status")
@CrossOrigin(origins = "http://localhost:4200")
public class StatusController {

    private final StatusService statusService;

    @Autowired
    public StatusController(StatusService statusService) {
        this.statusService = statusService;
    }

    @GetMapping
    public ResponseEntity<List<Status>> getStatus() {
        return new ResponseEntity<>(statusService.getStatus(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Status> createStatus(@RequestBody Status status) {
        return new ResponseEntity<>(statusService.createStatus(status), HttpStatus.CREATED);
    }

    @PutMapping("{name}")
    public ResponseEntity<Status> updateStatus(@PathVariable("name") String name, @RequestBody Status status) {
        return new ResponseEntity<>(statusService.updateStatus(name, status), HttpStatus.OK);
    }

    @DeleteMapping("{name}")
    public ResponseEntity<Status> deleteStatus(@PathVariable("name") String name) {
        statusService.deleteStatus(name);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
