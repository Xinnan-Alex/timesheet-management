package com.example.springapi.Configs;

import com.example.springapi.Models.Status;
import com.example.springapi.Models.User;
import com.example.springapi.Repo.StatusRepo;
import com.example.springapi.Repo.UserRepo;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class DataLoader {
    @Bean
    CommandLineRunner commandLineRunnerStatus(StatusRepo statusRepo, UserRepo userRepo){
        return args -> {
            Status closed  = new Status(
                    "Closed"
            );

            Status inProgress = new Status(
                    "In Progress"
            );

            Status open = new Status(
                    "Open"
            );

            statusRepo.saveAll(
                    List.of(closed,inProgress,open)
            );

            User user1 = new User(
                    "Leong Xin Nan"
            );

            User user2 = new User(
                    "Leong Xin Zhen"
            );

            userRepo.saveAll(
                    List.of(user1,user2)
            );
        };
    }
}
