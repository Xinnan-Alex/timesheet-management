# Timesheet Management System built Using Angular and Spring Boot

## Client

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 17.0.1. Bootstrap is used for css styling and builing the UI Componenets.

### Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

To run the client server, you can run `npm run start`

## Server

The backend is built using Java Spring Boot with JPA Data, MySQL is used for the database.

MySQL Docker Run Command:

```cmd
docker run --name mysql8-javaspringboot -p 3306:3306 -e MYSQL_ROOT_PASSWORD=secret -e -d mysql
```

### Endpoint

#### Timesheet

List Timesheets sortby either totime/fromtime, sort type can be either desc or asc  
**GET** <http://localhost:8080/api/v1/timesheets?sortType={sortType}&sortBy={sortBy}>

List Timesheets  
**GET** <http://localhost:8080/api/v1/timesheets>

Delete Timesheet by Id  
**DELETE** <http://localhost:8080/api/v1/timesheets/{id}>

Update Timesheet by Id  
**PUT** <http://localhost:8080/api/v1/timesheets/{id}>

Create Timesheet  
**POST** <http://localhost:8080/api/v1/timesheets>

```json
{
  "projectName": "project name",
  "taskName": "task name",
  "fromTime": "yyyy-MM-dd'T'HH:mm",
  "toTime": "yyyy-MM-dd'T'HH:mm",
  "status": {
    "name": "status name"
  },
  "user": {
    "id": 1
  }
}
```

#### Users

List Users  
**GET** <http://localhost:8080/api/v1/users>

Delete User by Id  
**DELETE** <http://localhost:8080/api/v1/users/{id}>

Update User by Id  
**PUT** <http://localhost:8080/api/v1/users/{id}>

Create User  
**POST** <http://localhost:8080/api/v1/users>

```json
{
  "fullName": "leongxinnan"
}
```

#### Status

List Status  
**GET** <http://localhost:8080/api/v1/status>

Delete Status by Name(PK)  
**DELETE** <http://localhost:8080/api/v1/status/{name}>

Update Status by Name(PK)
**PUT** <http://localhost:8080/api/v1/status/{id}>

Create Status
**POST** <http://localhost:8080/api/v1/status>

```json
{
  "name": "leongxinnan"
}
```
